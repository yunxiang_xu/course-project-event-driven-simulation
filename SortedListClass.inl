#include <iostream>
using namespace std;

#include "LinkedNodeClass.h"

//Allows the user to insert a value into the list.  Since this 
//is a sorted list, there is no need to specify where in the list
//to insert the element.  It will insert it in the appropriate
//location based on the value being inserted.  If the node value
//being inserted is found to be "equal to" one or more node values
//already in the list, the newly inserted node will be placed AFTER
//the previously inserted nodes.
template< class T >
void SortedListClass< T >::insertValue(
         const T &valToInsert  //The value to insert into the list
         )
{
  LinkedNodeClass< T > *newNode;
  bool           stopLooping;
  LinkedNodeClass< T > *tempNode;

  //If its the first element in a list...
  if (head == NULL)
  {
    newNode = new LinkedNodeClass< T >(NULL, valToInsert, NULL);
    head = newNode;
    tail = newNode;
  }
  else
  {
    stopLooping = false;
    tempNode = head;
    while (tempNode != NULL && !(valToInsert < tempNode->getValue()))
    {
      tempNode = tempNode->getNext();
    }

    //tempNode now points to the node AFTER the position where the new
    //node should be inserted.

    //If inserted to the very front of the list, update head.
    if (tempNode == head)
    {
      newNode = new LinkedNodeClass< T >(NULL, valToInsert, head);
      head = newNode;
    }
    else if (tempNode == NULL)
    {
      newNode = new LinkedNodeClass< T >(tail, valToInsert, NULL);
      tail = newNode;
    }
    else
    {
      newNode = new LinkedNodeClass< T >(tempNode->getPrev(),
                                       valToInsert, tempNode);
    }
  
    newNode->setBeforeAndAfterPointers();
  }
}

template< class T >
void SortedListClass< T >::printForward(
     ) const
{
  LinkedNodeClass< T > *tempNode;
  cout << "Forward List Contents Follow:" << endl;
  tempNode = head;
  while (tempNode != NULL)
  {
    cout << "  " << tempNode->getValue() << endl;
    tempNode = tempNode->getNext();
  }
  cout << "End Of List Contents" << endl;
}

template< class T >
void SortedListClass< T >::printBackward(
     ) const
{
  LinkedNodeClass< T > *tempNode;
  cout << "Backward List Contents Follow:" << endl;
  tempNode = tail;
  while (tempNode != NULL)
  {
    cout << "  " << tempNode->getValue() << endl;
    tempNode = tempNode->getPrev();
  }
  cout << "End Of List Contents" << endl;
}

template< class T >
bool SortedListClass< T >::removeFront(
     T &theVal
     )
{
  bool success;
  LinkedNodeClass< T > *tempPtr;
  success = true;

  if (head == NULL)
  {
    success = false;
  }
  else
  {
    tempPtr = head;
    theVal = head->getValue();
    head = head->getNext();
    delete tempPtr;

    if (head == NULL)
    {
      tail = NULL;
    }
    else
    {
      head->setPreviousPointerToNull();
    }
  }

  return (success);
}

template< class T >
bool SortedListClass< T >::removeLast(
     T &theVal
     )
{
  bool success;
  LinkedNodeClass< T > *tempPtr;
  success = true;

  if (tail == NULL)
  {
    success = false;
  }
  else
  {
    tempPtr = tail;
    theVal = tail->getValue();
    tail = tail->getPrev();
    delete tempPtr;

    if (tail == NULL)
    {
      head = NULL;
    }
    else
    {
      tail->setNextPointerToNull();
    }
  }

  return (success);
}

template< class T >
int SortedListClass< T >::getNumElems(
     ) const
{
  int                 nodeCtr;
  LinkedNodeClass< T > *tempPtr;

  tempPtr = head;

  nodeCtr = 0;

  while (tempPtr != NULL)
  {
    nodeCtr++;
    tempPtr = tempPtr->getNext();
  }

  return (nodeCtr);
}

template< class T >
bool SortedListClass< T >::getElemAtIndex(
     const int index,
     T &outVal
     )
{
  int i;
  bool success;
  LinkedNodeClass< T > *tempPtr;

  success = false;

  if (index >= 0 && index < this->getNumElems())
  {
    success = true;

    tempPtr = head;

    for (i = 0; i < index; i++)
    {
      tempPtr = tempPtr->getNext();
    }

    outVal = tempPtr->getValue();
  }
  return (success);
}

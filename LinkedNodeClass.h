#ifndef _LISTNODECLASS_H_
#define _LISTNODECLASS_H_

//The list node class will be the data type for individual nodes of
//a doubly-linked data structure.
template < class T >
class LinkedNodeClass
{
  private:
    LinkedNodeClass *prevNode; //Will point to the node that comes before
                               //this node in the data structure.  Will be
                               //NULL if this is the first node.
    T nodeVal;                 //The value contained within this node.
    LinkedNodeClass *nextNode; //Will point to the node that comes after
                               //this node in the data structure.  Will be
                               //NULL if this is the last node.

  public:
    //The ONLY constructor for the list node class - it takes in the
    //newly created node's previous pointer, value, and next pointer,
    //and assigns them.
    LinkedNodeClass(
         LinkedNodeClass * inPrev, //Node that comes before this one
         const T &inVal,                 //Value to be contained in this node
         LinkedNodeClass * inNext  //Node that comes after this one
         ):prevNode(inPrev), nodeVal(inVal), nextNode(inNext)
    { /*no statements necessary */ }

    //Returns the value stored within this node.
    T getValue(
         ) const;

    //Returns the address of the node that follows this node.
    LinkedNodeClass* getNext(
         ) const;

    //Returns the address of the node that comes before this node.
    LinkedNodeClass* getPrev(
         ) const;
    
    //Sets the object's next node pointer to the value passed in.
    void setNextPointerToNull(
         );

    //Sets the object's previous node pointer to the value passed in.
    void setPreviousPointerToNull(
         );

    //This function DOES NOT modify "this" node.  Instead, it uses
    //the pointers contained within this node to change the previous
    //and next nodes so that they point to this node appropriately.
    void setBeforeAndAfterPointers(
         );
};

#include "LinkedNodeClass.inl"

#endif // _LISTNODECLASS_H_

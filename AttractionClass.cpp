#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

//Programmer: Yunxiang Xu
//Date: April 2015
//Purpose: implenmentation of AttractionClass
#include "AttractionClass.h"

const int STD_INDEX= 0;
const int FP_INDEX= 1;
const int SFP_INDEX= 2;

void AttractionClass::setNumAddmitted(int numSFP, int numFP)
{
  numAddmitted[STD_INDEX]= NUM_OF_SEATS- numSFP- numFP;
  numAddmitted[FP_INDEX]= numFP;
  numAddmitted[SFP_INDEX]= numSFP;
}

void AttractionClass::addOneRider(int levelIndex, int comingTime)
{
  levelQueue[levelIndex].enqueue(comingTime);
}

bool AttractionClass::takeOneRide(int* inNum)
{                                           //add a ride and take some
  int numOfAllRider= 0;                     //riders
  int indexRider;
  bool fullRide= false;
  bool stateBoarding;
  for(int i= 0; i< NUM_OF_LEVELS; i++)      //first allcote riders based
  {                                         //on the number addmitted
    inNum[i]= 0;
    stateBoarding= true;
    while(stateBoarding)
    {
      stateBoarding= levelQueue[i].dequeue(indexRider);
      if(stateBoarding)
      {
        inNum[i]++;
        numOfAllRider++;
      }
      if(inNum[i]== numAddmitted[i])
      {
        stateBoarding= false;
      }
    }
  }
  if(numOfAllRider== NUM_OF_SEATS)
  {
    fullRide= true;
  }
  else
  {
    for(int i= NUM_OF_LEVELS- 1; i>= 0; i--)  //if the ride is not full
    {                                         //take some more riders
      if(!fullRide)                           //under some rules
      {
        stateBoarding= true;
        while(stateBoarding)
        {
          stateBoarding= levelQueue[i].dequeue(indexRider);
          if(stateBoarding)
          {
            inNum[i]++;
            numOfAllRider++;
          }
          if(numOfAllRider== NUM_OF_SEATS)
          {
            fullRide= true;
            stateBoarding= false;
          }
        }
      }
    }
  }
  return fullRide;
}

void AttractionClass::computeLineLength(int* inLine)
{
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    inLine[i]= levelQueue[i].getNumElems();
  }
}

bool AttractionClass::tellIfEmpty()
{
  int numRider= 0;
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    numRider= numRider+ levelQueue[i].getNumElems();
  }
  if(numRider== 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void AttractionClass::clear()
{
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    levelQueue[i].clear();
  }
}


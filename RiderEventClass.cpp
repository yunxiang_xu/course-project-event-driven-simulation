#include<iostream>
using namespace std;

//Programmer: Yunxiang Xu
//Date: April 2015
//Purpose: implenmentation of RiderEventClass
#include "RiderEventClass.h"

RiderEventClass::RiderEventClass()
{
  leavingTime= 0;
}

RiderEventClass::RiderEventClass(int cTime)
{
  comingTime= cTime;
  leavingTime= 0;
}

int RiderEventClass::getWaitingTime()
{
  return leavingTime- comingTime;
}

int RiderEventClass::getTimeCome() const
{
  return comingTime;
}

int RiderEventClass::getTimeLeave() const
{
  return leavingTime;
}

void RiderEventClass::setTimeLeave(int lTime)
{
  leavingTime= lTime;
} 

bool RiderEventClass::operator>(const RiderEventClass &in) const
{
  if(leavingTime!= in.getTimeLeave())
  {
    return leavingTime> in.getTimeLeave();
  }
  else
  {
    return comingTime> in.getTimeCome();
  }
}

bool RiderEventClass::operator<(const RiderEventClass &in) const
{
  if(leavingTime!= in.getTimeLeave())
  {
    return leavingTime< in.getTimeLeave();
  }
  else
  {
    return comingTime< in.getTimeCome();
  }
}

bool RiderEventClass::operator>=(const RiderEventClass &in) const
{
  if(leavingTime!= in.getTimeLeave())
  {
    return leavingTime>= in.getTimeLeave();
  }
  else
  {
    return comingTime>= in.getTimeCome();
  }
}

bool RiderEventClass::operator<=(const RiderEventClass &in) const
{
  if(leavingTime!= in.getTimeLeave())
  {
    return leavingTime<= in.getTimeLeave();
  }
  else
  {
    return comingTime<= in.getTimeCome();
  }
}

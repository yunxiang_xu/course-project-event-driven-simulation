#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

//Programmer: Yunxiang Xu
//Date: April 2015
//Purpose: Implentation of SimulationClass
#include "SimulationClass.h"

const int STD_INDEX= 0;
const int FP_INDEX= 1;
const int SFP_INDEX= 2;
const int NUM_OF_LEVELS= 3;
const int NUM_SEATS= 20;

SimulationClass::SimulationClass()
{
  numFullCars= numNonFullCars= 0;
  maxNumOneCar= 0;
  minNumOneCar= NUM_SEATS;
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    maxLength[i]= 0;
    maxNumPri[i]= 0;
    minNumPri[i]= NUM_SEATS;
  }
}

bool SimulationClass::readFromFile(string &inFname)
{
  int numSFP ;
  int numFP;
  ifstream inFile;
  inFile.open(inFname.c_str());
  if (inFile.fail())
  {
    inFile.clear();
    inFile.ignore(200, '\n');
    cout << "ERROR: Unable to open file" << endl;
    return false;
   }
  else
  {
    inFile>>closingTime;                  //read in the paramters
    inFile>>riderArrvialMean;
    inFile>>riderArrivalStddev;
    inFile>>carArrivalMin;
    inFile>>carArrivalMax;
    inFile>>percentageOfSFP;
    inFile>>percentageOfFP;
    inFile>>numSFP;
    inFile>>numFP;
    spaceMontain.setNumAddmitted(numSFP, numFP);
    if (inFile.fail())
    {
      inFile.clear();
      inFile.ignore(200, '\n');
      cout << "The file is damaged!" << endl;
      return false;
    }
    else
    {
      return true;
    }
  }
}
void SimulationClass::addOneRider(int &timeIndex)
{
  int chanceOfPass= getUniform(1,100);
  int lvlIndex;
  if(chanceOfPass<= percentageOfSFP)
  {                                     //print the process
    lvlIndex= SFP_INDEX;
    cout<<"At time: "<<timeIndex<<" One SuperFastPass Rider Comes"<<endl;
  }
  else if(chanceOfPass<= percentageOfFP+ percentageOfSFP)
  {
    lvlIndex= FP_INDEX;
    cout<<"At time: "<<timeIndex<<" One FastPass Rider Comes"<<endl;
  }
  else
  {
    lvlIndex= STD_INDEX;
    cout<<"At time: "<<timeIndex<<" One Standard Rider Comes"<<endl;
  }
  spaceMontain.addOneRider(lvlIndex, timeIndex);
  RiderEventClass riderArr(timeIndex);
  riderList[lvlIndex].insertValue(riderArr);
  int* lineLength= new int[NUM_OF_LEVELS];
  spaceMontain.computeLineLength(lineLength);
  for(int i= 0; i< NUM_OF_LEVELS; i++)      //compute some data every time
  {                                         //add a rider
    if(maxLength[i]< lineLength[i])
    {
      maxLength[i]= lineLength[i];
    }
  }
  delete [] lineLength;
  int timeStep;
  do
  {
    timeStep= getNormal(riderArrvialMean, riderArrivalStddev);
    timeIndex= timeIndex+ timeStep;
  }
  while(timeStep== 0);
}

bool SimulationClass::tellIfToRun()
{
  if(spaceMontain.tellIfEmpty())
  {                                   //if the attraction is empty, do not
    return false;                     //need to run, actually this function
  }                                   //is developped for multi attractions
  else                                //in the future. If all the attractions
  {                                   //are empty, theme park can stop
    return true;                      //operating
  }
}

bool SimulationClass::addCarDepart(int &timeIndex)
{
  int* inNum= new int[NUM_OF_LEVELS];
  int numOfAllRider= 0;
  bool state = spaceMontain.takeOneRide(inNum);
  cout<<"At time: "<<timeIndex<<" Car deaparting!"
      <<" STD: "<<inNum[STD_INDEX]
      <<" FP: "<<inNum[FP_INDEX]
      <<" SFP: "<<inNum[SFP_INDEX]<<endl;
  RiderEventClass riderLeave;
  for(int i= 0; i<NUM_OF_LEVELS; i++)
  {
    for(int j= 0; j<inNum[i]; j++)
    {
    riderList[i].removeFront(riderLeave);
    riderLeave.setTimeLeave(timeIndex);
    riderList[i].insertValue(riderLeave);
    }
    numOfAllRider= numOfAllRider+ inNum[i];
    if(maxNumPri[i]< inNum[i])          //compute some statistics every time
    {                                   //add a ride
      maxNumPri[i]= inNum[i];
    }
    if(minNumPri[i]> inNum[i])
    {
      minNumPri[i]= inNum[i];
    }
  }
  if(maxNumOneCar< numOfAllRider)
  {
    maxNumOneCar= numOfAllRider;
  }
  if(minNumOneCar> numOfAllRider)
  {
    minNumOneCar= numOfAllRider;
  }
  delete [] inNum;
  timeIndex = timeIndex + getUniform(carArrivalMin, carArrivalMax);
  return state;
}

void SimulationClass::simulateProcess()
{
  int operationTime;
  bool stateContiune= true;
  int timeRiderArrival=getNormal(riderArrvialMean,riderArrivalStddev);
  int timeCarArrival=getUniform(carArrivalMin, carArrivalMax);
  for (int timeStep = 0; timeStep <= closingTime; timeStep++)
  {
    if (timeStep == timeRiderArrival)
    {
      addOneRider(timeRiderArrival);
      
    }
    if (timeStep == timeCarArrival)
    {
      bool fullRide= addCarDepart(timeCarArrival);
      if(fullRide)
      {
        numFullCars++;
      }
      else
      {
        numNonFullCars++;
      }
    }
  }
  while(tellIfToRun())
  {
    bool fullRide= addCarDepart(timeCarArrival);
    if(fullRide)
    {
      numFullCars++;                  //compute some statistics during the 
    }                                 //process of the simulation
    else
    {
      numNonFullCars++;
    }
    operationTime= timeCarArrival;
  }
  cout<<"Total operation time: "<<operationTime<<endl;
}

void SimulationClass::printCarData()
{
  cout<<"Car departs "<<numFullCars+ numNonFullCars
      <<" times! "<<"Full cars: "<<numFullCars<<" Nonfull cars: "
      <<numNonFullCars<<endl;
  cout<<"Max number of riders in one car: "<<maxNumOneCar<<endl;
  cout<<"Min number of riders in one car: "<<minNumOneCar<<endl;
  cout<<"Number of STD riders in one car: Max: "<<maxNumPri[STD_INDEX]
      <<" Min: "<<minNumPri[STD_INDEX]<<endl;
  cout<<"Number of FP riders in one car: Max: "<<maxNumPri[FP_INDEX]
      <<" Min: "<<minNumPri[FP_INDEX]<<endl;
  cout<<"Number of SFP riders in one car: Max: "<<maxNumPri[SFP_INDEX]
      <<" Min: "<<minNumPri[SFP_INDEX]<<endl;
}

void SimulationClass::printRiderData()
{
  int maxWTime[NUM_OF_LEVELS];
  int minWTime[NUM_OF_LEVELS];
  double meanWTime[NUM_OF_LEVELS];
  int numRider[NUM_OF_LEVELS];
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    maxWTime[i]= 0;
    minWTime[i]= 0;
    meanWTime[i]= 0;
    numRider[i]= 0;
  }
  int numAllRiders= 0;
  RiderEventClass riderData;
  for(int i= 0; i< NUM_OF_LEVELS; i++)        //use the SortedListClass to 
  {                                           //compute some statistics
    numRider[i]= riderList[i].getNumElems();
    for(int j= 0; j< numRider[i]; j++)
    {
      riderList[i].removeFront(riderData);
      if(riderData.getWaitingTime()> maxWTime[i])
      {
        maxWTime[i]= riderData.getWaitingTime();
      }
      if(riderData.getWaitingTime()< minWTime[i])
      {
        minWTime[i]= riderData.getWaitingTime();
      }
      meanWTime[i]= meanWTime[i]+ riderData.getWaitingTime();
    }
      meanWTime[i]= meanWTime[i]/numRider[i];
      numAllRiders= numAllRiders+ numRider[i];
  }
  cout<<numAllRiders<<" riders come today! STD: "<<numRider[STD_INDEX]
      <<" FP: "<<numRider[FP_INDEX]<<" SFP: "<<numRider[SFP_INDEX]<<endl;
  cout<<"Standard riders waiting time: Max: "<<maxWTime[STD_INDEX]
      <<" Min: "<<minWTime[STD_INDEX]
      <<" Mean: "<<meanWTime[STD_INDEX]<<endl;
  cout<<"FastPass riders waiting time: Max: "<<maxWTime[FP_INDEX]
      <<" Min: "<<minWTime[FP_INDEX]
      <<" Mean: "<<meanWTime[FP_INDEX]<<endl;
  cout<<"SuperFastPass riders waiting time: Max: "<<maxWTime[SFP_INDEX]
      <<" Min: "<<minWTime[SFP_INDEX]
      <<" Mean: "<<meanWTime[SFP_INDEX]<<endl;
  cout<<"Max standard line length is: "<<maxLength[STD_INDEX]<<endl;
  cout<<"Max fastpass line length is: "<<maxLength[FP_INDEX]<<endl;
  cout<<"Max superfastpass line length is: "<<maxLength[SFP_INDEX]<<endl;
}

void SimulationClass::clear()
{
  for(int i= 0; i< NUM_OF_LEVELS; i++)
  {
    riderList[i].clear();
  }
  spaceMontain.clear();
}

#ifndef _RIDEREVENTCLASS_H_
#define _RIDEREVENTCLASS_H_


class RiderEventClass
{
private:
  //two time that indicate the time of coming and time of leaving
  int comingTime;
  int leavingTime;

public:
  //ctor
  RiderEventClass();
  RiderEventClass(int cTime);
  //some functionality that compute the waiting time and get or set
  //some data
  int getWaitingTime();
  int getTimeCome() const;
  int getTimeLeave() const;
  void setTimeLeave(int lTime);
  
  //overload operator to let the class be stored in the SortedListClass
  //effectively. We make the leaving time as first priority so that when
  //a rider is leaving, he/her node would get out of the list and can come
  //back at the last. We can all always use the node at the from the head.
  bool operator>(const RiderEventClass &in) const;
  bool operator<(const RiderEventClass &in) const;
  bool operator>=(const RiderEventClass &in) const;
  bool operator<=(const RiderEventClass &in) const;
  
};

#endif

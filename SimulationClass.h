#ifndef _SIMULATIONCLASS_H_
#define _SIMULATIONCLASS_H_

#include "AttractionClass.h"
#include "SortedListClass.h"
#include "RiderEventClass.h"
#include "random.h"

class SimulationClass //to represent a simulation process
{
private:
  //the paramters read from file
  int closingTime;
  double riderArrvialMean;
  double riderArrivalStddev;
  int carArrivalMin;
  int carArrivalMax;
  int percentageOfSFP;
  int percentageOfFP;
  static const int NUM_OF_LEVELS= 3;
  
  //the attraction in the simulation
  AttractionClass spaceMontain;
  //sortedlistclass that used to maintain the event
  SortedListClass<RiderEventClass> riderList[NUM_OF_LEVELS];
  
  //some statistics in the simulation
  int numFullCars;
  int numNonFullCars;
  int maxNumOneCar;
  int maxNumPri[NUM_OF_LEVELS];
  int minNumPri[NUM_OF_LEVELS];
  int minNumOneCar;
  int maxLength[NUM_OF_LEVELS];
public:
  //the ctor of the class  
  SimulationClass();
  
  //to read from a txt file which stored some paramters used in the
  //simulation
  bool readFromFile(string &inFname);
  
  //to add one rider in the simulation in the time timeIndex
  void addOneRider(int &timeIndex);
  
  //to see if the line is empty, if not, car still need to go because 
  //we are a nice theme park
  bool tellIfToRun();
  
  //to add a car in the simulation in the time timeIndex
  bool addCarDepart(int &timeIndex);
  
  //conduct the simulation process and get some basic data that need to be 
  //computed during the process of the simulation
  void simulateProcess();
  
  //compute some car data and print them out
  void printCarData();
  
  //compute some rider data using the SortedListClass and print them out
  void printRiderData();
  
  //clear the simulation to aviod memory leaks
  void clear();
};
#endif

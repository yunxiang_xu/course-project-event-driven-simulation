yxxu4: SimulationClass.o yxxu4.o random.o AttractionClass.o RiderEventClass.o
	g++ SimulationClass.o yxxu4.o random.o AttractionClass.o RiderEventClass.o -o yxxu4

yxxu4.o: yxxu4.cpp
	g++ -c yxxu4.cpp -o yxxu4.o

AttractionClass.o: AttractionClass.cpp AttractionClass.h FIFOQueueClass.h
	g++ -c AttractionClass.cpp -o AttractionClass.o

SimulationClass.o: SimulationClass.cpp SimulationClass.h SortedListClass.h RiderEventClass.h
	g++ -c SimulationClass.cpp -o SimulationClass.o

RiderEventClass.o: RiderEventClass.cpp RiderEventClass.h 
	g++ -c RiderEventClass.cpp -o RiderEventClass.o

random.o: random.cpp random.h
	g++ -c random.cpp -o random.o

clean:
	rm -rf yxxu4 yxxu4.o SimulationClass.o AttractionClass.o random.o RiderEventClass.o

#include <iostream>
using namespace std;

#include "FIFOQueueClass.h"

template<class T>
FIFOQueueClass<T>::FIFOQueueClass()
{
  head= NULL;
  tail= NULL;
}

template<class T>
void FIFOQueueClass<T>::clear()
{
  T temp;
  while(head!= NULL)
  {
    this->dequeue(temp);
  }
}

template<class T>
int FIFOQueueClass<T>::getNumElems()
{
  int numElems= 0;
  LinkedNodeClass<T>* tempPtr;
  tempPtr= head;
  while(tempPtr!= NULL)
  {
    tempPtr= tempPtr->getNext();
    numElems++;
  }
  return numElems;
}

template<class T>
void FIFOQueueClass<T>::enqueue(const T &newItem)
{
  if(head== NULL)
  {
    LinkedNodeClass<T>* newNode= new LinkedNodeClass<T>(0, newItem, 0);
    head= newNode;
    tail= newNode;
  }
  else
  {
    LinkedNodeClass<T>* newNode= new LinkedNodeClass<T>(tail, newItem, 0);
    tail= newNode;
    newNode->setBeforeAndAfterPointers();
  }

}

template<class T>
bool FIFOQueueClass<T>::dequeue(T &outItem)
{
  if(head== NULL)
  {
    return false;
  }
  else if(head->getNext()== NULL)
  {
    outItem= head->getValue();
    delete head;
    head= NULL;
    tail= NULL;
    return true;
  }
  else
  {
    outItem= head->getValue();
    head= head->getNext();
    delete head->getPrev();
    head->setPreviousPointerToNull();
    return true;
  }
}

template<class T>
void FIFOQueueClass<T>::print() const
{
  if(head== NULL)
  {
    cout<<"This is an empty line!"<<endl;
  }
  else
  {
    LinkedNodeClass<T>* nodePointer= head;
    while(nodePointer!= NULL)
    {
      cout<<nodePointer->getValue()<<" ";
      nodePointer= nodePointer->getNext();
    }
    cout<<endl;
  }
}
 

#ifndef _ATTRACTIONCLASS_H_
#define _ATTRACTIONCLASS_H_

#include "FIFOQueueClass.h"

class AttractionClass
{
private:
  //indicat the number of the levels of the passes
  int static const NUM_OF_LEVELS= 3;
  
  //an array indicating the number of the riders of each level that
  //can be addmitted in one ride
  int numAddmitted[NUM_OF_LEVELS];
  
  //an array of FIFOQueues to maintain the queues for each pass
  FIFOQueueClass<int> levelQueue[NUM_OF_LEVELS];
  
  //the fixed number of the seats
  int static const NUM_OF_SEATS= 20;

public:
  //to set the number of the riders of each level that can be admitted
  void setNumAddmitted(int numSFP, int numFP);
  
  //add one rider in the line
  void addOneRider(int levelIndex, int comingTime);

  //car departs and take some of the riders
  bool takeOneRide(int* inNum);

  //compute length of each line
  void computeLineLength(int* inLine);
  //to judge if all the queues are empty
  bool tellIfEmpty();
  
  //clear all the queues
  void clear();
};

#endif

This program is a simulation of an attration at a theme park such as Disneyland. At some theme parks, visitors can pay extra fee and get priority access to different attractions(standard, fastpass and super fastpass). With the paramters of the parks and visitors, it can output the simulation result of the park for one day.

The parameters of the parks are stored in spec.txt as follows, change as you want:
<closing time> 
<rider arrival mean>
<rider arrival stddev>
<car arrival min>
<car arrival max>
<percentage of riders that are super fast pass>
<percentageof riders that are fast pass>
<number of super fast pass riders admitted>
<number offast pass riders admitted>

To run the program, type the following commands in the terminal:
make
./yxxu4 spec.txt

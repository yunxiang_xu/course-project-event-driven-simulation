#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

//Programmer: Yunxiang Xu
//Date: April 2015
//Purpose: Main function of project 4
#include "SimulationClass.h"

int main(int argc, char* argv[])
{
  //read from console, if there is nothing ,return the warning.
  string inFname;
  if(argc== 1)
  {
    cout<<"Please add the file name."<<endl;
  }
  else
  {
    inFname= argv[1];
  }
  //conduct the simulation
  SimulationClass parkSim;
  if(parkSim.readFromFile(inFname))
  {
  parkSim.simulateProcess();
  cout<<"Cars Statistics:"<<endl;
  parkSim.printCarData();
  cout<<"Riders Statistics:"<<endl;
  parkSim.printRiderData();
  parkSim.clear();
  }
  //if filename is wrong or file is damaged, return the warning.
  else
  {
    cout<<"Simulation can not be done!"<<endl;
  }
}
